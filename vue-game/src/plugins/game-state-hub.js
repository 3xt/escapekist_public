import {
	HubConnectionBuilder,
	LogLevel,
	HttpTransportType
} from "@microsoft/signalr";
export default {
	install(Vue) {
		var baseUrl = `http://${window.location.hostname}:${process.env.VUE_APP_SIGNALR_PORTNR}/gameStateHub`;
		const connection = new HubConnectionBuilder()
			.withUrl(baseUrl, {
				skipNegotiation: true,
				transport: HttpTransportType.WebSockets
			})
			.configureLogging(LogLevel.Information)
			.build();

		// use new Vue instance as an event bus
		const bus = new Vue();
		// every component will use this.$gameStateHub to access the event bus
		Vue.prototype.$bus = bus;
		// Forward server side SignalR events through $gameStateHub, where components will listen to them
		connection.on("Time", state => {
			bus.$emit("time", state);
		});
		connection.on("GameStarted", state => {
			bus.$emit("game-started", state);
		});
		connection.on("GameEnded", state => {
			bus.$emit("game-ended", state);
		});
		connection.on("GamePaused", state => {
			bus.$emit("game-paused", state);
		});
		connection.on("GameResumed", state => {
			bus.$emit("game-resumed", state);
		});
		connection.on("GameQuestion", state => {
			bus.$emit("game-question", state);
		});
		connection.on("GameQuestionSuccess", state => {
			bus.$emit("game-question-success", state);
		});
		connection.on("GameQuestionFailure", state => {
			bus.$emit("game-question-failure", state);
		});
		connection.on("On", state => {
			bus.$emit("on", state);
		});
		connection.on("Off", state => {
			bus.$emit("off", state);
		});
		connection.on("Up", state => {
			bus.$emit("up", state);
		});
		connection.on("Down", state => {
			bus.$emit("down", state);
		});

		let startedPromise = null;
		function start() {
			startedPromise = connection.start().catch(() => {
				// console.error("Failed to connect with hub", err);
				return new Promise((resolve, reject) =>
					setTimeout(
						() =>
							start()
								.then(resolve)
								.catch(reject),
						5000
					)
				);
			});
			return startedPromise;
		}
		connection.onclose(() => start());

		start();

		// Provide methods for components to send messages back to server
		// Make sure no invocation happens until the connection is established
		bus.bigButtonPressed = () => {
			if (!startedPromise) return;

			return startedPromise.then(() => connection.invoke("BigButtonPressed"));
			//.catch(console.error);
		};
		bus.buttonPressed = id => {
			if (!startedPromise) return;

			return startedPromise.then(() => connection.invoke("ButtonPressed", id));
			//.catch(console.error);
		};
		bus.buttonReleased = id => {
			if (!startedPromise) return;

			return startedPromise.then(() => connection.invoke("ButtonReleased", id));
			//.catch(console.error);
		};
	}
};
