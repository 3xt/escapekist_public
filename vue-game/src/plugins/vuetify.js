import Vue from "vue";
import Vuetify from "vuetify/lib";

Vue.use(Vuetify);

export default new Vuetify({
	theme: {
		themes: {
			light: {
				primary: "#02b3e2",
				secondary: "#eb0b28",
				accent: "#eb0b28",
				error: "#b71c1c",
				info: "#2196F3",
				success: "#4CAF50",
				warning: "#FFC107"
			}
		}
	},
	icons: {
		iconfont: "mdi"
	}
});
