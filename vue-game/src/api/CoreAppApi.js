import { baseApi } from "@/api/BaseApi";

export default {
	ledOn: async function() {
		return baseApi.get("BlinkingLed/LedOn");
	},
	ledOff: async function() {
		return baseApi.get("BlinkingLed/LedOff");
	},
	timeLeft: async function() {
		return baseApi.get("Game/TimeLeft");
	},
	startGame: async function(filename) {
		return baseApi.get(`Game/Start/${filename}`);
	},
	getGames: async function() {
		return baseApi.get("Game/FileList");
	}
};
