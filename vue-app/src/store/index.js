import Vue from "vue";
import Vuex from "vuex";
import { ADD_CORE_APP_LOG, ADD_CORE_GPIO_LOG } from "@/store/mutations-types";

Vue.use(Vuex);

// state
const state = {
	coreAppLog: [],
	coreGpioLog: []
};

// actions
const actions = {
	/*
	addCoreAppLog: (context, payload) => {
		context.commit(ADD_CORE_APP_LOG, payload);
  }
  */
};

// mutations
const mutations = {
	[ADD_CORE_APP_LOG]: function(state, data) {
		state.coreAppLog.unshift(data);
		if (state.coreAppLog.length > 100) state.coreAppLog.pop();
	},
	[ADD_CORE_GPIO_LOG]: function(state, data) {
		state.coreGpioLog.unshift(data);
		if (state.coreGpioLog.length > 100) state.coreGpioLog.pop();
	}
};

export default new Vuex.Store({
	state: state,
	getters: {},
	mutations: mutations,
	actions: actions,
	modules: {}
});
