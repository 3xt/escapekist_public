import {
	HubConnectionBuilder,
	LogLevel,
	HttpTransportType
} from "@microsoft/signalr";

import store from "../store";

// options.url, options.mutation
const SignalRCoreAppPlugin = {
	install(Vue, options) {
		// make your signalR connection
		var baseUrl = options.url;
		const connection = new HubConnectionBuilder()
			.withUrl(baseUrl, {
				skipNegotiation: true,
				transport: HttpTransportType.WebSockets
			})
			.configureLogging(LogLevel.Information)
			.build();
		connection.logging = true;
		let startedPromise = null;
		function start() {
			startedPromise = connection.start().catch(() => {
				// console.error("Failed to connect with hub", err);
				return new Promise((resolve, reject) =>
					setTimeout(
						() =>
							start()
								.then(resolve)
								.catch(reject),
						5000
					)
				);
			});
			return startedPromise;
		}
		connection.onclose(() => start());

		start();
		connection.on("PushEventLog", state => {
			// store.dispatch("addCoreAppLog", state);
			store.commit(options.mutation, state);
		});

		//Anything you want to expose outside of the plugin should be prefixed Vue.prototype.$
		/*
		Vue.prototype.$sendSomething = function(something) {
			connection.send("Incoming", something);
		};
		*/
	}
};

export default SignalRCoreAppPlugin;
