import Vue from "vue";
import App from "./App.vue";
import "./registerServiceWorker";
import router from "./router";
import store from "./store";
import vuetify from "./plugins/vuetify";
import GameStateHub from "./plugins/game-state-hub";
import SignalRCoreAppPlugin from "./plugins/signalr-core-app";
import { ADD_CORE_APP_LOG, ADD_CORE_GPIO_LOG } from "@/store/mutations-types";

Vue.config.productionTip = false;

Vue.use(GameStateHub);
Vue.use(SignalRCoreAppPlugin, {
	url: `http://${window.location.hostname}:${process.env.VUE_APP_SIGNALR_PORTNR}/log`,
	mutation: ADD_CORE_APP_LOG
});
Vue.use(SignalRCoreAppPlugin, {
	url: `http://${window.location.hostname}:${process.env.VUE_GPIO_SIGNALR_PORTNR}/log`,
	mutation: ADD_CORE_GPIO_LOG
});

Vue.mixin({
	methods: {
		navigate: function(routename, params) {
			this.$router.push({
				name: routename,
				params
			});
			// this.$router.push({ path: `/${i18n.locale}/${routename}`, params });
		},
		// https://stackoverflow.com/a/36114029/1981247
		sortByTerm: function(data, key, term) {
			return data.sort(function(a, b) {
				// cast to lowercase
				return a[key].toLowerCase().indexOf(term) <
					b[key].toLowerCase().indexOf(term)
					? -1
					: 1;
			});
		},
		sortArr: function(data, key, desc) {
			return data.sort(function(a, b) {
				// cast to lowercase
				if (desc) {
					return a[key] > b[key] ? -1 : 1;
				} else {
					return a[key] < b[key] ? -1 : 1;
				}
			});
		}
	}
});

new Vue({
	router,
	store,
	vuetify,
	render: h => h(App)
}).$mount("#app");
