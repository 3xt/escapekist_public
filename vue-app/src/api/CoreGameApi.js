import axios from "axios";

const baseApi = axios.create({
	// baseURL: "http" + "//" + "raspberrypi" + ":" + "5001"
	baseURL: `http://${window.location.hostname}:${process.env.VUE_APP_SIGNALR_PORTNR}`
	// baseURL: location.protocol + "//" + "raspberrypi" + ":" + "5001"
	// baseURL: location.protocol + "//" + location.hostname + ":" + process.env.PORT
	//  baseURL: process.env.HOST:process.env.PORT,
});

export default {
	systems: async function() {
		return baseApi.get(`/Game/Systems`);
	},
	fileList: async function() {
		return baseApi.get(`/Game/FileList`);
	},
	deleteFile: async function(filename) {
		return baseApi.delete(`/Game/${filename}`);
	},
	new: async function(name, game) {
		return baseApi.post(`/Game/${name}`, game);
	},
	get: async function(filename) {
		return baseApi.get(`/Game/${filename}`);
	},
	update: async function(name, game) {
		return baseApi.put(`/Game/${name}`, game);
	},
	uploadFile: async function(formData) {
		return baseApi.post(`/Game/UploadFile`, formData, {
			headers: {
				"Content-Type": "multipart/form-data"
			}
		});
	}
};
