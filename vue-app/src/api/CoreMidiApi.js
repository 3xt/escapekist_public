import axios from "axios";

const baseApi = axios.create({
	// baseURL: "http" + "//" + "raspberrypi" + ":" + "5001"
	baseURL: `http://${window.location.hostname}:${process.env.VUE_APP_SIGNALR_PORTNR}`
	// baseURL: location.protocol + "//" + "raspberrypi" + ":" + "5001"
	// baseURL: location.protocol + "//" + location.hostname + ":" + process.env.PORT
	//  baseURL: process.env.HOST:process.env.PORT,
});

export default {
	getInstruments: async function() {
		return baseApi.get(`/Midi/Instruments`);
	},
	getPercussions: async function() {
		return baseApi.get(`/Midi/Percussions`);
	},
	playNote: async function(input) {
		return baseApi.get(
			`/Midi/PlayNote?note=${input.note}&milliseconds=${input.milliseconds}&velocity=${input.velocity}&instrument=${input.instrument}`
		);
	},
	playPercussion: async function(input) {
		return baseApi.get(
			`/Midi/PlayPercussion?milliseconds=${input.milliseconds}&velocity=${input.velocity}&percussion=${input.percussion}`
		);
	},
	playFile: async function(input) {
		// input.fileName, input.milliseconds
		return baseApi.post("/Midi/Playfile", input);
	},
	fileList: async function() {
		return baseApi.get(`/Midi/FileList`);
	},
	deleteFile: async function(filename) {
		return baseApi.delete(`/Midi/${filename}`);
	},
	uploadFile: async function(formData) {
		return baseApi.post(`/Midi/UploadFile`, formData, {
			headers: {
				"Content-Type": "multipart/form-data"
			}
		});
	}
};
