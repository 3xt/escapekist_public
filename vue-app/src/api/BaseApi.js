import axios from "axios";

export const baseApi = axios.create({
  baseURL: location.protocol + "//" + location.hostname + ":" + "5000"
  // baseURL: location.protocol + "//" + location.hostname + ":" + process.env.PORT
  //  baseURL: process.env.HOST:process.env.PORT,
});
// baseURL: process.env.baseURL || process.env.apiUrl || ""
// timeout: 60 * 1000, // Timeout
// withCredentials: true, // Check cross-site Access-Control
