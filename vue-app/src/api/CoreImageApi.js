import axios from "axios";

const baseApi = axios.create({
  // baseURL: "http" + "//" + "raspberrypi" + ":" + "5001"
  baseURL: `http://${window.location.hostname}:${process.env.VUE_APP_SIGNALR_PORTNR}`
  // baseURL: location.protocol + "//" + "raspberrypi" + ":" + "5001"
  // baseURL: location.protocol + "//" + location.hostname + ":" + process.env.PORT
  //  baseURL: process.env.HOST:process.env.PORT,
});

export default {
  get: async function() {
    return baseApi.get(`/Image/${filename}`);
  },
  fileList: async function() {
    return baseApi.get(`/Image/FileList`);
  },
  deleteFile: async function(filename) {
    return baseApi.delete(`/Image/${filename}`);
  },
  uploadFile: async function(formData) {
    return baseApi.post(`/Image/UploadFile`, formData, {
      headers: {
        "Content-Type": "multipart/form-data"
      }
    });
  }
};
