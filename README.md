# Escape kist

- [Architecture](#Architecture)
- [Docker](./.docs/Docker.md)
- [Midi](./.docs/Midi.md)
- [Remote development](./.docs/RemoteDev.md)
- [i2c-bus](./.docs/i2c.md)
- [The comprehensive GPIO Pinout guide for the Raspberry Pi.](./.docs/PiPins.md)

## Architecture

![Architecture](./.docs/images/architecture.svg)

`export DOCKER_HOST="tcp://raspberrypi:2375"`

`unset DOCKER_HOST`

`docker-compose up -d`

`docker-compose up -d --no-deps --build <service_name>`

`docker-compose -f docker-compose.yml -f docker-compose.Development.yml up -d --no-deps --build coreapp`

`docker run -it --privileged --device /dev/snd pythonapp /bin/bash`

`docker run -it --privileged -e PULSE_SERVER=docker.for.mac.localhost -v ~/.config/pulse:/home/pulseaudio/.config/pulse pythonapp /bin/bash`
