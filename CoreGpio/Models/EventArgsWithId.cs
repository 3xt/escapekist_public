using System;

namespace CoreGpio.Models
{
	public class EventArgsWithId : EventArgs
	{
		public int Id { get; set; }
	}
}