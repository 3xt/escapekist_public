using System;
using CoreGpio.Hubs;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog;
using Serilog.Sinks.SignalR.Core.Extensions;
using Serilog.Sinks.SignalR.Core.Interfaces;

namespace CoreGpio
{
	public class Startup
	{
		private readonly IHostEnvironment _env;

		public Startup(IConfiguration configuration, IHostEnvironment env)
		{
			Configuration = configuration;
			_env = env;

			Log.Logger = new LoggerConfiguration()
				.ReadFrom.Configuration(Configuration)
				.CreateLogger();
		}

		public IConfiguration Configuration { get; }

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
		{
			services.AddCors(options =>
			{
				// this defines a CORS policy called "default"
				options.AddPolicy("corsPolicy", policy =>
				{
					policy
					//.WithOrigins()
					.WithOrigins("http://raspberrypi:8080", "http://raspberrypi2:8080")
					.AllowAnyHeader()
					.SetIsOriginAllowed((host) => true)
					.AllowAnyMethod()
					.AllowCredentials();
				});
			});

			services.AddHostedService<Worker>();
			services.AddSignalR();
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IServiceProvider sp)
		{
			app.UseRouting();

			app.UseCors("corsPolicy");

			// Build an intermediate service provider
			var hub = sp.GetService<IHubContext<SerilogHub, ISerilogHub>>();
			Log.Logger = new LoggerConfiguration()
				.MinimumLevel.Information()
				.WriteTo.SignalR(hub, null, null, null, null)
				.CreateLogger();

			app.UseEndpoints(endpoints =>
			{
				endpoints.MapHub<SerilogHub>("/log");
			});
		}
	}
}
