// using System;
// using System.Device.Gpio;
// using System.Device.I2c;
// using System.Threading;
// using System.Threading.Tasks;
// using Iot.Device.Mcp23xxx;

// namespace CoreGpio.Peripherals
// {
// 		/// <summary>
// 		/// Leds with Mcp23017
// 		/// </summary>
// 		public class Leds
// 		{
// 				private readonly int _deviceAddress;

// 				private Mcp23017 _mcp;

// 				public Leds(int deviceAddress = 0x21)
// 				{
// 						_deviceAddress = deviceAddress;
// 				}

// 				private byte _ledStateA = 0b0000_0000;
// 				private byte _ledStateB = 0b0000_0000;

// 				public async void StartListening(CancellationToken token)
// 				{
// 						try
// 						{
// 								var i2cConnectionSettings = new I2cConnectionSettings(2, _deviceAddress);
// 								var i2cDevice = I2cDevice.Create(i2cConnectionSettings);

// 								using (_mcp = new Mcp23017(i2cDevice))
// 								{
// 										Console.WriteLine("Write LEDs");
// 										var controller = new GpioController(PinNumberingScheme.Logical, _mcp);

// 										// Output direction for LEDs.
// 										_mcp.WriteByte(Register.IODIR, 0b0000_0000, Port.PortA);
// 										_mcp.WriteByte(Register.IODIR, 0b0000_0000, Port.PortB);

// 										while (!token.IsCancellationRequested)
// 										{
// 												await Task.Delay(100);
// 										}
// 								}

// 						}
// 						catch (Exception e)
// 						{
// 								Console.WriteLine($"Exception {e.Message}");
// 								if (_mcp != null)
// 								{
// 										_mcp.Dispose();
// 								}
// 						}
// 				}

// 				public void LedOn(int id)
// 				{
// 						if (id <= 8)
// 						{
// 								// Port A, zero based
// 								id -= 1;
// 								//left-shift 1, then bitwise OR
// 								var newState = (byte)(_ledStateA | (1 << id));
// 								_ledStateA = newState;
// 								_mcp.WriteByte(Register.GPIO, newState, Port.PortA);
// 						}
// 						else
// 						{
// 								// Port B, zero based
// 								id -= 9;
// 								//left-shift 1, then bitwise OR
// 								var newState = (byte)(_ledStateB | (1 << id));
// 								_ledStateB = newState;
// 								_mcp.WriteByte(Register.GPIO, newState, Port.PortB);
// 						}
// 				}

// 				public void LedOff(int id)
// 				{
// 						if (id <= 8)
// 						{
// 								// Port A, zero based
// 								id -= 1;
// 								//left-shift 1, then bitwise OR
// 								var newState = (byte)(_ledStateA & ~(1 << id));
// 								_ledStateA = newState;
// 								_mcp.WriteByte(Register.GPIO, newState, Port.PortA);
// 						}
// 						else
// 						{
// 								// Port B, zero based
// 								id -= 9;
// 								//left-shift 1, then bitwise OR
// 								var newState = (byte)(_ledStateB & ~(1 << id));
// 								_ledStateB = newState;
// 								_mcp.WriteByte(Register.GPIO, newState, Port.PortB);
// 						}
// 				}
// 		}
// }