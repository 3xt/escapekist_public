using System;
using System.Device.Gpio;
using System.Device.I2c;
using System.Threading;
using System.Threading.Tasks;
using CoreGpio.Models;
using Iot.Device.Mcp23xxx;

namespace CoreGpio.Peripherals
{
		/// <summary>
		/// ButtonRow with Mcp23017
		/// </summary>
		public class ButtonRow
		{
				private readonly int _deviceAddress;

				private Mcp23017 _mcp;

				public ButtonRow(int deviceAddress = 0x20)
				{
						_deviceAddress = deviceAddress;
				}

				private byte _ledState = 0b0000_0000;

				public async void StartListening(CancellationToken token)
				{
						try
						{
								Console.WriteLine("Stap 1");
								var i2cConnectionSettings = new I2cConnectionSettings(1, _deviceAddress);
								Console.WriteLine("Stap 2");
								var i2cDevice = I2cDevice.Create(i2cConnectionSettings);

								Console.WriteLine("Stap 3");
								using (_mcp = new Mcp23017(i2cDevice))
								{
										Console.WriteLine("Stap 4");
										var controller = new GpioController(PinNumberingScheme.Logical, _mcp);
										Console.WriteLine("Read Switches & Write LEDs");

										// Input direction for switches.
										_mcp.WriteByte(Register.IODIR, 0b1111_1111, Port.PortA);
										await Task.Delay(200);

										// Output direction for LEDs.
										_mcp.WriteByte(Register.IODIR, 0b0000_0000, Port.PortB);
										await Task.Delay(200);

										var oldByte = (byte)0;
										while (!token.IsCancellationRequested)
										{
												try
												{
														var newByte = _mcp.ReadByte(Register.GPIO, Port.PortA);
														if (newByte != oldByte)
														{
																for (var bitNumber = 1; bitNumber <= 8; bitNumber++)
																{
																		var newBit = (newByte & (1 << bitNumber - 1)) != 0;
																		var oldBit = (oldByte & (1 << bitNumber - 1)) != 0;
																		if (newBit != oldBit)
																		{
																				if (newBit)
																				{
																						Console.WriteLine($"pressed: {bitNumber}");
																						HandleButtonPressed(bitNumber);
																				}
																				else
																				{
																						Console.WriteLine($"released: {bitNumber}");
																						HandleButtonReleased(bitNumber);
																				}
																		}
																}

																oldByte = newByte;
														}
												}
												catch
												{
														Console.WriteLine("Exception reading buttons");
												}

												await Task.Delay(200);
										}

										// ReadBits(controllerUsingMcp);
								}

						}
						catch (Exception e)
						{
								Console.WriteLine($"Exception {e.Message}");
								if (_mcp != null)
								{
										_mcp.Dispose();
								}
						}
				}

				public void LedOn(int id)
				{
						// zero based
						id -= 1;

						//left-shift 1, then bitwise OR
						var newState = (byte)(_ledState | (1 << id));
						_ledState = newState;
						_mcp.WriteByte(Register.GPIO, newState, Port.PortB);
				}

				public void LedOff(int id)
				{
						// zero based
						id -= 1;

						//left-shift 1, then take complement, then bitwise AND
						var newState = (byte)(_ledState & ~(1 << id));
						_ledState = newState;
						_mcp.WriteByte(Register.GPIO, newState, Port.PortB);
				}

				/// <summary>
				/// Occurs when [pressed].
				/// </summary> 
				public event EventHandler<EventArgsWithId> Pressed;

				/// <summary>
				/// Occurs when [released].
				/// </summary>
				public event EventHandler<EventArgsWithId> Released;

				private void HandleButtonPressed(int id)
				{
						Pressed?.Invoke(this, new EventArgsWithId { Id = id });
				}

				private void HandleButtonReleased(int id)
				{
						Released?.Invoke(this, new EventArgsWithId { Id = id });
				}
		}
}