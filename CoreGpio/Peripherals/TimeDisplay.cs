using System;
using System.Device.I2c;
using Iot.Device.Display;

namespace CoreGpio.Peripherals
{
		public class TimeDisplay : IDisposable
		{
				private readonly int _busId;

				private readonly Large4Digit7SegmentDisplay _display;

				public TimeDisplay(int busId = 1)
				{
						_busId = busId;

						// Initialize display (busId = 1 for Raspberry Pi 2 & 3)
						_display = new Large4Digit7SegmentDisplay(I2cDevice.Create(new I2cConnectionSettings(busId: _busId, Ht16k33.DefaultI2cAddress)))
						{
								// Set max brightness
								Brightness = Ht16k33.MaxBrightness
						};
						// Write "Iot" on the display
						_display.Write("GO !");

				}

				public void DisplayTime(int seconds)
				{
						_display.Write($"{seconds / 60:D2}:{ seconds % 60:D2}");
				}


				public void Dispose()
				{
						_display.Dispose();
				}
		}
}