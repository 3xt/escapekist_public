// using System;
// using System.Device.Gpio;
// using System.Device.I2c;
// using System.Threading;
// using System.Threading.Tasks;
// using CoreGpio.Models;
// using Iot.Device.Mcp23xxx;

// namespace CoreGpio.Peripherals
// {
// 		/// <summary>
// 		/// Buttons with Mcp23017
// 		/// </summary>
// 		public class Buttons
// 		{
// 				private readonly int _deviceAddress;

// 				private Mcp23017 _mcp;

// 				public Buttons(int deviceAddress = 0x20)
// 				{
// 						_deviceAddress = deviceAddress;
// 				}

// 				public async void StartListening(CancellationToken token)
// 				{
// 						try
// 						{
// 								var i2cConnectionSettings = new I2cConnectionSettings(1, _deviceAddress);
// 								var i2cDevice = I2cDevice.Create(i2cConnectionSettings);

// 								using (_mcp = new Mcp23017(i2cDevice))
// 								{
// 										Console.WriteLine("Read Buttons");
// 										var controller = new GpioController(PinNumberingScheme.Logical, _mcp);

// 										// Input direction for buttons.
// 										_mcp.WriteByte(Register.IODIR, 0b1111_1111, Port.PortA);
// 										_mcp.WriteByte(Register.IODIR, 0b1111_1111, Port.PortB);

// 										var oldByteA = (byte)0;
// 										var oldByteB = (byte)0;
// 										while (!token.IsCancellationRequested)
// 										{
// 												try
// 												{
// 														var newByteA = _mcp.ReadByte(Register.GPIO, Port.PortA);
// 														oldByteA = CheckByteChange(newByteA, oldByteA, 0);
// 														var newByteB = _mcp.ReadByte(Register.GPIO, Port.PortB);
// 														oldByteA = CheckByteChange(newByteB, oldByteB, 8);
// 												}
// 												catch
// 												{
// 														Console.WriteLine("Exception reading buttons");
// 												}

// 												await Task.Delay(100);
// 										}

// 										// ReadBits(controllerUsingMcp);
// 								}

// 						}
// 						catch (Exception e)
// 						{
// 								Console.WriteLine($"Exception {e.Message}");
// 								if (_mcp != null)
// 								{
// 										_mcp.Dispose();
// 								}
// 						}
// 				}

// 				private byte CheckByteChange(byte newByte, byte oldByte, int offset = 0)
// 				{
// 						if (newByte != oldByte)
// 						{
// 								for (var bitNumber = 1; bitNumber <= 8; bitNumber++)
// 								{
// 										var newBit = (newByte & (1 << bitNumber - 1)) != 0;
// 										var oldBit = (oldByte & (1 << bitNumber - 1)) != 0;
// 										if (newBit != oldBit)
// 										{
// 												var buttonId = bitNumber + offset;
// 												if (newBit)
// 												{
// 														Console.WriteLine($"pressed: {buttonId}");
// 														HandleButtonPressed(buttonId);
// 												}
// 												else
// 												{
// 														Console.WriteLine($"released: {buttonId}");
// 														HandleButtonReleased(buttonId);
// 												}
// 										}
// 								}

// 								oldByte = newByte;
// 						}

// 						return oldByte;
// 				}

// 				/// <summary>
// 				/// Occurs when [pressed].
// 				/// </summary> 
// 				public event EventHandler<EventArgsWithId> Pressed;

// 				/// <summary>
// 				/// Occurs when [released].
// 				/// </summary>
// 				public event EventHandler<EventArgsWithId> Released;

// 				private void HandleButtonPressed(int id)
// 				{
// 						Pressed?.Invoke(this, new EventArgsWithId { Id = id });
// 				}

// 				private void HandleButtonReleased(int id)
// 				{
// 						Released?.Invoke(this, new EventArgsWithId { Id = id });
// 				}
// 		}
// }