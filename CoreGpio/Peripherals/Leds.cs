using System;
using System.Threading;
using ABElectronicsUK;
using CoreGpio.Models;

namespace CoreGpio.Peripherals
{
		/// <summary>
		/// Write Leds with IO Pi Plus.
		/// </summary>
		public class Leds : IDisposable
		{
				private readonly byte _deviceAddress;
				private IOPi _bus;
				private CancellationToken _token;

				public Leds(byte deviceAddress = 0x21)
				{
						_deviceAddress = deviceAddress;
				}

				public void StartListening(CancellationToken token)
				{
						_token = token;

						try
						{
								_bus = new IOPi(_deviceAddress);
								_bus.Connected += IOPi_Connected;
								_bus.Connect();
						}
						catch (Exception e)
						{
								Console.WriteLine($"Exception {e.Message}");
						}
				}

				private void IOPi_Connected(object sender, EventArgs e)
				{
						// We will write the pins 1 to 16 from the I/O bus so set port 0 and
						// port 1 to be outputs and enable the internal pull-up resistors
						_bus.SetPortDirection(0, 0x00);
						_bus.SetPortDirection(1, 0x00);

						// turn off the pins
						_bus.WritePort(0, 0x00);
						_bus.WritePort(1, 0x00);

						// disable the internal pull-up resistors
						_bus.SetPortPullups(0, 0xFF);
						_bus.SetPortPullups(1, 0xFF);
				}

				public void LedOn(int id)
				{
						_bus.WritePin((byte)id, true);
				}

				public void LedOff(int id)
				{
						_bus.WritePin((byte)id, false);
				}

				public void Dispose()
				{
						// turn off the pins
						_bus.WritePort(0, 0x00);
						_bus.WritePort(1, 0x00);
				}
		}
}