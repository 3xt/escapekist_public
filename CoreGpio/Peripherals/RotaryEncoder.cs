using System;
using System.Device.Gpio;
using System.Threading;
using System.Threading.Tasks;

namespace CoreGpio.Peripherals
{
		public class RotaryEncoder
		{
				private const int Sleeptime = 50;
				private readonly int _pinClk;
				private readonly int _pinDt;

				private GpioController _controller;

				public RotaryEncoder(int pinClk = 23, int pinDt = 24)
				{
						_pinClk = pinClk;
						_pinDt = pinDt;
				}

				public async Task StartListening(CancellationToken token)
				{
						using (_controller = new GpioController())
						{
								_controller.OpenPin(_pinClk, PinMode.Input);
								_controller.OpenPin(_pinDt, PinMode.Input);
								var oneLastState = _controller.Read(_pinClk);
								while (!token.IsCancellationRequested)
								{
										var state = _controller.Read(_pinClk);
										if (state != oneLastState)
										{
												if (_controller.Read(_pinDt) != state)
												{
														HandleRotateUp();
												}
												else
												{
														HandleRotateDown();
												}
												// oneLastState = twoState;
												await Task.Delay(Sleeptime);
										}
								}

						}
				}

				/// <summary>
				/// Occurs when [rotate right].
				/// </summary> 
				public event EventHandler<EventArgs> Up;

				/// <summary>
				/// Occurs when [rotate left].
				/// </summary>
				public event EventHandler<EventArgs> Down;

				private void HandleRotateUp()
				{
						Up?.Invoke(this, new EventArgs());
				}

				private void HandleRotateDown()
				{
						Down?.Invoke(this, new EventArgs());
				}
		}
}