using System;
using System.Device.Gpio;
using System.Threading;

namespace CoreGpio.Peripherals
{
		public class Led
		{
				private readonly int _pinNumber;
				private GpioController _controller;

				public Led(int pinNumber)
				{
						_pinNumber = pinNumber;
				}

				public void LedOn()
				{
						using (_controller = new GpioController())
						{
								_controller.OpenPin(_pinNumber, PinMode.Output);
								_controller.Write(_pinNumber, PinValue.High);
						}
				}

				public void LedOff()
				{
						using (_controller = new GpioController())
						{
								_controller.OpenPin(_pinNumber, PinMode.Output);
								_controller.Write(_pinNumber, PinValue.Low);
						}
				}

				public void LedBlink(int lightTimeInMilliseconds = 1000, int dimTimeInMilliseconds = 200)
				{
						using (_controller = new GpioController())
						{
								while (true)
								{
										_controller.Write(_pinNumber, PinValue.High);
										Thread.Sleep(lightTimeInMilliseconds);
										_controller.Write(_pinNumber, PinValue.Low);
										Thread.Sleep(dimTimeInMilliseconds);
								}
						}
				}
		}
}