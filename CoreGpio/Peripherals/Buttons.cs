using System;
using System.Threading;
using System.Threading.Tasks;
using ABElectronicsUK;
using CoreGpio.Models;

namespace CoreGpio.Peripherals
{
		/// <summary>
		/// Read Buttons with IO Pi Plus.
		/// </summary>
		public class Buttons : IDisposable
		{
				private readonly byte _deviceAddress;
				private IOPi _bus;
				private CancellationToken _token;

				private bool[] _buttonState = { true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true };

				public Buttons(byte deviceAddress = 0x20)
				{
						_deviceAddress = deviceAddress;
				}

				public void StartListening(CancellationToken token)
				{
						_token = token;

						try
						{
								_bus = new IOPi(_deviceAddress);
								_bus.Connected += async (s, e) => await IOPi_ConnectedAsync(s, e);
								_bus.Connect();
						}
						catch (Exception e)
						{
								Console.WriteLine($"Exception {e.Message}");
						}
				}

				private async Task IOPi_ConnectedAsync(object sender, EventArgs e)
				{
						// We will read the inputs 1 to 16 from the I/O bus so set port 0 and
						// port 1 to be inputs and enable the internal pull-up resistors
						_bus.SetPortDirection(0, 0xFF);
						_bus.SetPortPullups(0, 0xFF);

						_bus.SetPortDirection(1, 0xFF);
						_bus.SetPortPullups(1, 0xFF);

						// clear the console
						Console.Clear();

						while (true)
						{
								// read the pins 1 to 16 on both buses and print the results
								for (var i = 1; i < 17; i++)
								{
										var value = _bus.ReadPin((byte)i);
										if (_buttonState[i - 1] != value)
										{
												if (value)
												{
														// true = high = released
														Console.WriteLine($"Button released {i}");
														Released?.Invoke(this, new EventArgsWithId { Id = i });
												}
												else
												{
														// false = low = pressed
														Console.WriteLine($"Button pressed {i}");
														Pressed?.Invoke(this, new EventArgsWithId { Id = i });
												}
												_buttonState[i - 1] = value;
										}
								}

								// await Task.Delay(200);
						}
				}

				/// <summary>
				/// Occurs when [pressed].
				/// </summary> 
				public event EventHandler<EventArgsWithId> Pressed;

				/// <summary>
				/// Occurs when [released].
				/// </summary>
				public event EventHandler<EventArgsWithId> Released;

				public void Dispose()
				{
				}
		}
}