using System;
using System.Device.Gpio;
using System.Threading;

namespace CoreGpio.Peripherals
{
	public class Button
	{
		private const int ButtonSleeptime = 300;
		private readonly int _pinNumber;
		private readonly PinMode _pinMode;

		private GpioController _controller;

		public Button(int pinNumber, PinMode pinMode)
		{
			_pinMode = pinMode;
			_pinNumber = pinNumber;
		}

		public void StartListening(CancellationToken token)
		{
			using (_controller = new GpioController())
			{
				_controller.OpenPin(_pinNumber, PinMode.Input);
				while (!token.IsCancellationRequested)
				{
					if (_controller.Read(_pinNumber) == ((_pinMode == PinMode.InputPullDown) ? PinValue.Low : PinValue.High))
					{
						HandleInterrupt();
						Thread.Sleep(ButtonSleeptime);
					}
				}
			}
		}

		/// <summary>
		/// Occurs when [pressed].
		/// </summary> 
		public event EventHandler<EventArgs> Pressed;

		/// <summary>
		/// Occurs when [released].
		/// </summary>
		public event EventHandler<EventArgs> Released;

		private void HandleInterrupt()
		{ /*
			var val = _gpioPin.Read();

			if ((val && _gpioPin.InputPullMode == GpioPinResistorPullMode.PullDown) ||
				(!val && _gpioPin.InputPullMode == GpioPinResistorPullMode.PullUp))
				HandleButtonPressed();
			else
				HandleButtonReleased();
				*/
			HandleButtonPressed();
		}

		private void HandleButtonPressed()
		{
			Pressed?.Invoke(this, new EventArgs());
		}

		private void HandleButtonReleased()
		{
			Released?.Invoke(this, new EventArgs());
		}
	}
}