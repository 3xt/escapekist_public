using System;
using System.Device.Gpio;
using System.Threading;
using System.Threading.Tasks;
using CoreGpio.Peripherals;
using Microsoft.AspNetCore.SignalR.Client;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace CoreGpio
{
		public class Worker : BackgroundService
		{
				private readonly ILogger<Worker> _logger;

				private IHostEnvironment _env;

				private HubConnection hub;

				private TimeDisplay _timeDisplay;

				private Buttons _buttons;
				private Leds _leds;
				private RotaryEncoder _rotary1;
				private RotaryEncoder _rotary2;

				public Worker(ILogger<Worker> logger, IHostEnvironment env)
				{
						_logger = logger;
						_env = env;
				}

				public override async Task StartAsync(CancellationToken cancellationToken)
				{
						_logger.LogInformation($"StartAsync {_env.EnvironmentName}");
						string hubUrl;

						if (_env.IsDevelopment())
						{
								hubUrl = "http://localhost:5000/gameStateHub";
						}
						else
						{
								hubUrl = "http://coreapp:80/gameStateHub";
						}

						hub = new HubConnectionBuilder()
							.WithUrl(hubUrl)
							.WithAutomaticReconnect()
							.Build();

						// Keep trying to until we can start or the token is canceled.
						var connected = false;
						while (!connected)
						{
								try
								{
										await hub.StartAsync();
										connected = true;
										_logger.LogInformation($"SignalR connected to {hubUrl}");
								}
								catch
								{
										// Failed to connect, trying again in 5000 ms.
										_logger.LogError($"Connecting {hubUrl} failed. Retry in 5000 ms");
										await Task.Delay(5000);
								}
						}

						hub.Closed += async (error) =>
						{
								await Task.Delay(new Random().Next(0, 5) * 1000);
								await hub.StartAsync();
						};

						hub.On<object>("GameStarted", GameStarted);
						hub.On<object>("GameQuestionSuccess", GameQuestionSuccess);
						hub.On<object>("GameQuestionFailure", GameQuestionfailure);
						hub.On<object>("GameQuestion", GameQuestion);
						hub.On<object>("GamePaused", GamePaused);
						_logger.LogInformation("hub");

						// RotaryEncoder 1
						_rotary1 = new RotaryEncoder(23, 24);
						_rotary1.Down += async (s, e) => await hub.InvokeAsync("RotaryRotateDown", 0);
						_rotary1.Up += async (s, e) => await hub.InvokeAsync("RotaryRotateUp", 0);
						var rotary1Task = Task.Run(() => _rotary1.StartListening(cancellationToken));
						_logger.LogInformation("_rotary1");

						// RotaryEncoder 2
						_rotary2 = new RotaryEncoder(5, 6);
						_rotary2.Down += async (s, e) => await hub.InvokeAsync("RotaryRotateDown", 1);
						_rotary2.Up += async (s, e) => await hub.InvokeAsync("RotaryRotateUp", 1);
						var rotary2Task = Task.Run(() => _rotary2.StartListening(cancellationToken));
						_logger.LogInformation("_rotary2");

						// TimeDisplay
						try
						{
								_timeDisplay = new TimeDisplay();
						}
						catch (Exception e)
						{
								_logger.LogError(e, "Problem creating TimeDisplay");
						}
						hub.On<int>("Time", (secondsLeft) =>
						{
								_logger.LogInformation($"{secondsLeft}");
								_timeDisplay.DisplayTime(secondsLeft);
						});
						_logger.LogInformation("_timeDisplay");

						// Buttons
						_buttons = new Buttons();
						_ = Task.Run(() => _buttons.StartListening(cancellationToken));
						_buttons.Pressed += async (s, e) => await hub.InvokeAsync("ButtonPressed", e.Id);
						_buttons.Released += async (s, e) => await hub.InvokeAsync("ButtonReleased", e.Id);
						_logger.LogInformation("_buttons");

						// Leds
						_leds = new Leds();
						_ = Task.Run(() => _leds.StartListening(cancellationToken));
						hub.On<int>("On", (id) =>
						{
								_leds.LedOn(id);
						});
						hub.On<int>("Off", (id) =>
						{
								_leds.LedOff(id);
						});
						_logger.LogInformation("_leds");
				}

				private void GameStarted(object model)
				{
						_logger.LogInformation("Worker GameStarted event received");
				}

				private void GamePaused(object model)
				{
						_logger.LogInformation("Worker GamePaused event received");
				}

				private void GameQuestionSuccess(object model)
				{
						_logger.LogInformation("Worker GameQuestionSuccess event received");
				}

				private void GameQuestionfailure(object model)
				{
						_logger.LogInformation("Worker GameQuestionFailure event received");
				}

				private void GameQuestion(object model)
				{
						_logger.LogInformation("Worker GameQuestion event received");
				}


				// protected override async Task ExecuteAsync(CancellationToken stoppingToken)
				protected override Task ExecuteAsync(CancellationToken stoppingToken)
				{

						// var ledOn = true;.IsCancellationRequested)
						{
								_logger.LogInformation("Worker running at: {time}", DateTimeOffset.Now);
								// _blinkingLedService.DoWork(ledOn);;
						}

						return Task.CompletedTask;
				}

				public override Task StopAsync(CancellationToken stoppingToken)
				{
						_logger.LogInformation("StopAsync");

						return Task.CompletedTask;
				}

				public override void Dispose()
				{
						_logger.LogInformation("Dispose");
						_timeDisplay?.Dispose();
						_buttons?.Dispose();
						_leds?.Dispose();
				}
		}
}