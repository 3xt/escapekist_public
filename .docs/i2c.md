# i2c-bus

- Multiple devices can be attached to the same i2c bus (GPIO2-pin3, GPIO3-pin5). Devices must have a different addres (configurable on device).
- Enable i2c on Raspberry Pi via

  `sudo raspi-config`

- Test

  `lsmod | grep i2c_`

- Enable i2c system device to docker, or remove all restrictions with the privileged flag::

  `docker run --device /dev/i2c-0 --device /dev/i2c-1 myimage`

  `docker run --privileged myimage`

# Utilities

- Install

```bash
sudo apt-get update
sudo apt-get install -y python-smbus i2c-tools
```

- Test

  `sudo i2cdetect -y 1`
