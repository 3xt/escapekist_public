# MCP23017 i2c Port Expander

- [io-pi-plus](https://www.abelectronics.co.uk/p/54/io-pi-plus#assembly)
- [dotnet/iot](https://github.com/dotnet/iot/blob/master/src/devices/Mcp23xxx/README.md)
- [HowTo](https://www.raspberrypi-spy.co.uk/2013/07/how-to-use-a-mcp23017-i2c-port-expander-with-the-raspberry-pi-part-3/)

![](./images/Mcp23S17_I2c_ReadSwitches_WriteLeds.png)

# Connect

- Pin 9 (VDD) is connected to 3.3V
- Pin 10 (VSS) is connected to Ground
- Pin 12 (SCL) is connected to Pin 5 on the Pi GPIO
- Pin 13 (SDA) is connected to Pin 3 on the Pi GPIO
- Pin 18 (Reset) should be set high for normal operation so we connect this to 3.3V
- Pins 15, 16 and 17 (A0-A2) determine the number assigned to this device. We are only using one device so we will give it a binary zero by setting all three of these pins to 0 (ground)
