# Big button (Arcade button)

[kiwi](https://www.kiwi-electronics.nl/grote-arcade-knop-met-led-100mm-rood?search=arcade&description=true)

Deze knop heeft een platte plastic kap met een zwarte borgring er omheen. Er is een optionele LED meegeleverd dat de knop kan oplichten. De LED heeft een ingebouwde weerstand, zodat de knop kan werken tot 12V, maar geringe diepte van de knop is niet de hele knop gelijkmatig te verlichten. De knop schakelt een veel voorkomende arcade microschakelaar (inbegrepen). De schakelaar heeft een NO (normally-open) contact en een NC (normally-closed) contact.

Sluit de knoppen eenvoudig aan met quick connect draden. Gebruik 4.8mm voor de schakelaar en 6.35mm voor de LED.
