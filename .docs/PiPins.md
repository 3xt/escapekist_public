# Raspberry Pi Pinout

[The comprehensive GPIO Pinout guide for the Raspberry Pi.](https://pinout.xyz/pinout)

![PiOut](./images/PiOut.png)
