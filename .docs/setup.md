# Setup

## Clone image

[link](https://beebom.com/how-clone-raspberry-pi-sd-card-windows-linux-macos/)

## Download Raspbian Buster Lite image

- Download Raspbian for Raspberry Pi
  Raspbian is the Foundation's official supported operating system. You can install it with NOOBS or download the image below.
  [https://www.raspberrypi.org/downloads/raspbian/](https://www.raspberrypi.org/downloads/raspbian/)

## Download and install Balena Etcher, flash image

- [https://www.balena.io/etcher/](https://www.balena.io/etcher/)
- Us Etcher to flash the image to microSD

## Put some files in the boot partition of the microSD

- To enable ssh connections, create an empty file named "ssh"
- To connect to your local wifi network, create a file named "wpa_supplicant.conf". Put the next lines in the file, and change the values with your network credentials

```ssh
country=US
ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
update_config=1

network={
    ssid="your_real_wifi_ssid"
    scan_ssid=1
    psk="your_real_password"
    key_mgmt=WPA-PSK
}
```

## Boot Raspberry Pi

- Put microSD into raspberry pi and connect to power.
- Find the raspberry pi IP-address from your router. Default name of the pi is "raspberrypi"
- Translate the IP-address to a hostname (raspberrypi) by modifying your local hosts file
  - Mac:
  ```
  sudo nano /etc/hosts
  ```
  - Windows, open with notepad as administrator
  ```
  C:\Windows\System32\drivers\etc\hosts
  ```

# Use Visual Studio Code to connect with Raspberry Pi

- Download vscode
- Install extension pack "Remote Development"
- Make bash the default shell inside vscode
  - Terminal: Select Default Shell
    - (git)bash
- Default Pi credentials: pi/raspberry
- Remote-SSH: Connect to Host
  - ssh pi@raspberrypi -A
  - accept the fingerprint (first time only)
  - visual studio code will install the server part automaticly
- You can use both vscode terminal and explorer on the Raspberry Pi

## Update Raspberry Pi and install docker

- Run next commands on Pi

```bash
sudo apt-get install apt-transport-https ca-certificates software-properties-common -y
sudo apt update
sudo apt full-upgrade
curl -fsSL https://get.docker.com -o get-docker.sh
bash get-docker.sh
```

- Enable dockerd to listen on TCP also (Warning: this is basicly unsecure / anything can connect to port 2375)

```bash
sudo mkdir /etc/systemd/system
sudo mkdir /etc/systemd/system/docker.service.d
sudo vi /etc/systemd/system/docker.service.d/remote-api.conf
Past following content:
  '
  [Service]
  ExecStart=
  ExecStart=/usr/bin/dockerd -H tcp://0.0.0.0:2375 -H unix:///var/run/docker.sock
  '
Press ESC --> : --> wq! --> Enter to safe. for non VI users!
```

- Enable serial device see i2c.md
- Enable sound card on right port see midi.md
- Enable Touchscreen. see Touchscreen.md

## Docker

- Install Docker Desktop on your development machine.
  - change advanced settings -> 4 CPUs / 8192 MB, Shared Drives
  - check drives where repository is on. (In windows you have to create a specific user because your Azure user account will not succeed?)

## Other

- htop
- tail -f /var/log/syslog
- [Pi, Docker, Vpn](https://www.youtube.com/watch?v=a6mjt8tWUws)
- Disable Swap
- Install log2ram
- Install docker
- Run docker compose-up
