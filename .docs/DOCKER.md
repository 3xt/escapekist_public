# Docker

- Translate the IP-address to a hostname (raspberrypi) by modifying your local hosts file
  - Mac:
    `sudo nano /etc/hosts`
  - Windows, open with notepad as administrator
    `C:\Windows\System32\drivers\etc\hosts`
- Use the vscode Docker tools on the Raspberry Pi by setting the docker.host in .vscode/settings.json
  `"docker.host": "tcp://raspberrypi:2375",`
- Run docker commands in the vscode terminal on the Raspberry Pi

  `export DOCKER_HOST="tcp://raspberrypi:2375"`

  `unset DOCKER_HOST`
