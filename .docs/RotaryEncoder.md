# Rotary Encoder PCB

![](./images/RotaryEncoderPcb.jpg)

- [hackerstore](https://www.hackerstore.nl/Artikel/609)

# Connect #1

- \+ is connected to 3.3V
- GND is connected to Ground
- CLK (output1 Encoder) is connected to Pin 23 on the Pi GPIO
- DK (output2 Encoder) is connected to Pin 24 on the Pi GPIO
- SW (output Button)

# Connect #2

- \+ is connected to 3.3V
- GND is connected to Ground
- CLK (output1 Encoder) is connected to Pin 5 on the Pi GPIO
- DK (output2 Encoder) is connected to Pin 6 on the Pi GPIO
- SW (output Button)

# Extra info

- Connect a 0,47µ capacitor from ground to CLK (debouncing)
- Connect a 0,47µ capacitor from ground to DT (debouncing)
- Connect a 10 KiloOhm resistor from +5V to SW (no integrated pullup for SW !!)
