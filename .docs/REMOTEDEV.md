# DotNetCore 3.1 Development Inside a Container

## Dotnet

- dotnet run --environment="Production"

## Networks

- If connections to other containers is necessary, use Portainer to connect the development container to other networks

## Bitbucket ssh

- git
  ```
  git config --global user.email "you@example.com"
  git config --global user.name "Your Name"
  ```
- The Dockerfile contains statements for generating Bitbucker ssh keys, you should add the public key to Bitbucket

  - Output the key and copy/paste it into the Bitbucket website

  ```
  cat ~/.ssh/id_rsa.pub
  ```

  - Accept the fingerprint

  ```
  ssh bitbucket.org
  ```

- When the container is rebuild, the whole ssh procedure starts all over again ;-(

## Tips

- The development container will default be in the local docker host.
- Specify another docker host inside the settings.json
  `"docker.host": "tcp://raspberrypi:2375",`
- A local volume is created where the workspace is put. The workspace will stay available between sessions, even when the container is rebuild.
- When you want the workspace to be available from outside the container, you can use a "bind" volume.
- The settings for the Development Container are specified inside `".devcontainer/devcontainer.json"`
- You can specify a Dockerfile OR docker-compose file.
- You can specify a list with all vscode extensions that will be available inside the container.
