# Time display (4-Digit 7-Segment Display)

- [dotnet/iot](https://github.com/dotnet/iot/blob/master/src/devices/Display/README.md)
- [HowTo](https://learn.adafruit.com/matrix-7-segment-led-backpack-with-the-raspberry-pi)
- [kiwi](https://www.kiwi-electronics.nl/1-4cm-4-Digit-7-Segment-Display-met-I2C-Backpack-Rood?search=7%20digit&description=true)

# Connect

- VCC is connected to 3.3V
- GND is connected to Ground
- SCL is connected to Pin 5 on the Pi GPIO
- SDA is connected to Pin 3 on the Pi GPIO
