# MIDI

- http://www.music-software-development.com/midi-tutorial.html
- https://soundprogramming.net/file-formats/general-midi-drum-note-numbers/
- http://www.binaryconvert.com/result_unsigned_char.html?hexadecimal=99

* https://stackoverflow.com/questions/15863534/playing-note-with-pygame-midi/31855756
* http://cs.indstate.edu/~jkinne/cs151-f2012/code/midiExample.py

* The currently official pygame 1.9.6 is not compatible with Python 3.8, but there's a 2.0.0.dev6 version which is somewhat OK but still not enabled as available module for pip

```python
import pygame.midi
import time

pygame.midi.init()
player = pygame.midi.Output(0)
player.set_instrument(0)
player.note_on(64, 64)
time.sleep(1)
player.note_off(64, 64)
del player
pygame.midi.quit()
```

## bare linux container

- Install alsa

```bash
apt-get update
apt-get install libasound2 alsa-utils alsa-oss
apt-get install -y libsdl1.2-dev libsdl-image1.2-dev libsdl-mixer1.2-dev libsdl-ttf2.0-dev
```

## Linux

- aplay -l
- cat /proc/asound/cards
- aplay /usr/share/sounds/alsa/Front_Center.wav
- aplay /usr/share/sounds/alsa/Front_Center.wav -D sysdefault:CARD=0
- sudo reboot
- Change soundcard for all users: `sudo nano /etc/asound.conf`
- (asound.conf is already part of the coreapp image)

  ```bash
    	pcm.!default {
    	type plug slave {
    		pcm "hw:1,0"
    	}
    	}

    	ctl.!default {
    	type hw card 1
    	}
  ```

## Timidity

- [link](https://wiki.archlinux.org/index.php/Timidity)
- install: `apt-get install timidity`
- run: `timidity -iA -Os plughw:0`
- info: `aplaymidi -l`
- info: `aconnect -i`
- connect `aconnect 14:0 128:0`

## Mac

- qsynth
- soundfonts on mac can be found here: `/opt/local/share/sounds/sf2/`

- https://github.com/frescobaldi/frescobaldi/wiki/MIDI-playback-on-Mac-OS-X
- Install pulseaudio: `brew install pulseaudio`
- Run pulseaudio deamon: `pulseaudio --load=module-native-protocol-tcp --exit-idle-time=-1 --daemon`
- Check pulsaudio running: `pulseaudio --check -v`
- Test sound, run example container:
  `docker run -it -e PULSE_SERVER=docker.for.mac.localhost -v ~/.config/pulse:/home/pulseaudio/.config/pulse --entrypoint speaker-test --rm jess/pulseaudio -c 2 -l 1 -t wav`

## Hifiberry Amp

- Add next line in boot/config.txt: `dtoverlay=hifiberry-dac`
