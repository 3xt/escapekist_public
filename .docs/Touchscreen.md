# Touchscreen

- [kiwi](https://www.kiwi-electronics.nl/7-inch-raspberry-pi-dsi-touchscreen-display)

# HowTo

- Rotate screen 180 degrees
  - sudo nano /boot/config.txt: `lcd_rotate=2`
- Hiding the mouse cursor, install unclutter:
  - `sudo apt-get -y install unclutter`
- Autostart, disabling screen blanking, hiding the cursor and starting Chromium fullscreen
  - `sudo nano /etc/xdg/lxsession/LXDE-pi/autostart`
- Disable screen blanking

```
@xset s 0 0
@xset s noblank
@xset s noexpose
@xset dpms 0 0 0
```

- Hide the cursor

```
@unclutter -idle 0
```

- Start the browser

```
chromium-browser --kiosk --incognito --disable-features=WebRtcHideLocalIpsWithMdns http://localhost
```
