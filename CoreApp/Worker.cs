/*
using System;
using System.Threading;
using System.Threading.Tasks;
using CoreApp.Services;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using CoreApp.Hubs;
using CoreApp.Models;
using Microsoft.Extensions.Options;

namespace CoreApp
{
	public class Worker : BackgroundService
	{
		private readonly ILogger<Worker> _logger;

		private GameService _game;

		private IServiceProvider _serviceProvider;

		public Worker(ILogger<Worker> logger, IServiceProvider serviceProvider)
		{
			_logger = logger;
			_serviceProvider = serviceProvider;
		}

		public override async Task StartAsync(CancellationToken cancellationToken)
		{
			_logger.LogInformation("StartAsync");

			GameStateHub.BigButtonPressedEvent += async (s, e) => await BigButtonPressed(s, e);
			// GameStateHub.TimeEndedEvent += TimeEnded;
		}

		protected override async Task ExecuteAsync(CancellationToken stoppingToken)
		{

			// var ledOn = true;.IsCancellationRequested)
			{
				_logger.LogInformation("Worker running at: {time}", DateTimeOffset.Now);
			}
		}

		private async Task BigButtonPressed(object sender, EventArgsWithId e)
		{
			if (!GameStateHub.GamePaused)
			{
				_game = (GameService)_serviceProvider.GetService(typeof(GameService));
				var task = Task.Run(() => _game.Trigger());
			}
		}
	}
}
*/
