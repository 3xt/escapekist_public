#!/bin/bash

# turn on bash's job control
set -m

timidity -iA -Os plughw:0 > /dev/null &

sleep 4
aplaymidi -l
aconnect 14:0 128:0

dotnet CoreApp.dll