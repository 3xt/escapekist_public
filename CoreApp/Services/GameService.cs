using System;
using System.IO;
using System.Text.Json;
using System.Threading.Tasks;
using CoreApp.Hubs;
using CoreApp.Models;
using CoreApp.Models.GameEvents;
using CoreApp.Services.Systems;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace CoreApp.Services
{

		public class GameService
		{
				private readonly ILogger<GameService> _logger;
				private readonly AppSettings _settings;

				private readonly MidiService _midi;

				private readonly IServiceProvider _serviceProvider;

				private readonly IHubContext<GameStateHub, IGameStateHub> _hub;

				private TimeSystem _timeSystem;

				private GameModel _game;

				private int _currentQuestion = 0;

				private ISystem _currentSystem = null;

				private bool _isStarted = false;

				public GameService(ILogger<GameService> logger, IHubContext<GameStateHub, IGameStateHub> hub, MidiService midi, IServiceProvider serviceProvider, IOptions<AppSettings> settings)
				{
						_logger = logger;
						_hub = hub;
						_midi = midi;
						_settings = settings.Value;
						_serviceProvider = serviceProvider;
						_logger.LogInformation("### GameService Started ###");

						GameStateHub.TimeEndedEvent += async (s, e) => await TimeEnded(s, e); ;

						GameStateHub.BigButtonPressedEvent += BigButtonPressed;

				}

				private void BigButtonPressed(object sender, EventArgsWithId e)
				{
						if (_isStarted && !GameStateHub.GamePaused)
						{
								_ = Task.Run(() => Trigger());
						}
				}

				private GameModel LoadGame(string filename)
				{
						var filePath = Path.Combine(_settings.GamePath, filename);
						var jsonString = File.ReadAllText(filePath);
						return JsonSerializer.Deserialize<GameModel>(jsonString);
				}

				public async Task StartGame(string filename)
				{
						if (_isStarted)
						{
								await DoStopGame();
						}
						_game = LoadGame(filename);

						_timeSystem = (TimeSystem)_serviceProvider.GetService(typeof(TimeSystem));
						_timeSystem.Start(_game.TimeInMinutes * 60);
						_timeSystem.TimeEnded += async (s, e) => await TimeEnded(s, e);

						// Start the GAME!
						_isStarted = true;

						var startLeds = (SystemLedStartup)_serviceProvider.GetService(typeof(SystemLedStartup));
						_ = Task.Run(() => startLeds.StartAsync());

						// Play intro sound
						await _hub.Clients.All.GameStarted(_game.StartEvent);
						await _midi.PlayMusic(_game.StartEvent.Sound);
						await _hub.Clients.All.GameResumed();

						// Ask first Question
						await DoQuestion();
				}

				public async Task Trigger()
				{
						try
						{
								// Play frightning music
								await _hub.Clients.All.GamePaused(_game.WaitEvent);
								await _midi.PlayMusic(_game.WaitEvent.Sound);
								// await Task.Delay(1000);
								await _hub.Clients.All.GameResumed();

								// Check answer
								// var answerOk = await DoQuestionResult();
								var success = _currentSystem.CheckAnswer();
								_currentSystem.Dispose();

								if (success)
								{
										await _hub.Clients.All.GameQuestionSuccess(_game.SuccessEvent);
										// _ = Task.Run(() => _midi.PlaySuccessSound());
										// next question
										_currentQuestion++;
										if (_currentQuestion == _game.Questions.Count)
										{
												// success! Yeah
												await DoStopGame(true);
												await _hub.Clients.All.GameEnded(new GameEndedModel { });
										}
										else
										{
												// Only if next question else play EndEvent sound
												if (_game.SuccessEvent.Sound != null && !string.IsNullOrEmpty(_game.SuccessEvent.Sound.Filename))
												{
														await _midi.PlayMusic(_game.SuccessEvent.Sound);
												}
												else
												{
														_midi.PlaySuccessSound();
												}

												await DoQuestion();
										}
								}
								else
								{
										await _hub.Clients.All.GameQuestionFailure(_game.FailureEvent);
										if (_game.FailureEvent.Sound != null && !string.IsNullOrEmpty(_game.FailureEvent.Sound.Filename))
										{
												await _midi.PlayMusic(_game.FailureEvent.Sound);
										}
										else
										{
												_midi.PlayFailureSound();
										}
										// _ = Task.Run(() => _midi.PlayFailureSound());
										// same question again
										await DoQuestion();
								}
						}
						catch (Exception e)
						{
								_logger.LogError(e, "GameService Error.");
								// throw;
						}
				}

				private async Task TimeEnded(object sender, EventArgs e)
				{
						_logger.LogInformation($"TimeEnded");
						await DoStopGame();
						_timeSystem.Dispose();
				}

				private async Task DoQuestion()
				{
						var currentQuestion = _game.Questions[_currentQuestion];
						var model = new GameQuestionModel
						{
								Title = currentQuestion.Title,
								Text = currentQuestion.Text,
								Total = _game.Questions.Count,
								Current = _currentQuestion + 1,
								System = currentQuestion.System.ToString(),
								Image = currentQuestion.ImageName
						};

						_currentSystem = SystemResolver.GetSystem(_serviceProvider, currentQuestion.System);
						if (_currentSystem == null)
						{
								await DoStopGame();
						}
						else
						{
								_currentSystem.StartAsync(currentQuestion);
								await _hub.Clients.All.GameQuestion(model);
								if (currentQuestion.IntroSound?.Filename != null)
								{
										_ = Task.Run(() => _midi.PlayMusic(currentQuestion.IntroSound));
								}
						}
				}

				public async Task DoStopGame(bool success = false)
				{
						_currentQuestion = 0;
						if (_currentSystem != null) _currentSystem.Dispose();
						_currentSystem = null;
						_isStarted = false;
						if (_timeSystem != null) _timeSystem.Dispose();

						if (success)
						{
								await _hub.Clients.All.GameEnded(_game.EndEvent);
								await _midi.PlayMusic(_game.EndEvent.Sound);
						}
						else
						{
								await _hub.Clients.All.GameEnded(_game.EndEvent);
								// await _midi.PlayMusic(_game.EndEvent.Sound);
						}

						await _hub.Clients.All.GameEnded(_game.EndEvent);
						_logger.LogInformation("--> Game ended!!!");
				}
		}
}