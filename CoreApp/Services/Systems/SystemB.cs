using System.Collections.Generic;
using System.Linq;
using CoreApp.Hubs;
using CoreApp.Models;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;

namespace CoreApp.Services.Systems
{
		public class SystemB : ISystem
		{
				private readonly IHubContext<GameStateHub, IGameStateHub> _hub;

				private readonly MidiService _midi;

				private readonly ILogger<SystemB> _logger;

				private QuestionModel _question;

				private readonly List<int> _pressedButtons = new List<int>();

				private readonly List<int> _rotaryButton = new List<int> { 0, 0 };

				private readonly InstrumentsEnum[] _instruments = new InstrumentsEnum[] { InstrumentsEnum.Accordion, InstrumentsEnum.ChurchOrgan, InstrumentsEnum.Helicopter, InstrumentsEnum.BaritoneSax };

				public SystemB(ILogger<SystemB> logger, MidiService midi, IHubContext<GameStateHub, IGameStateHub> hub)
				{
						_logger = logger;
						_hub = hub;
						_midi = midi;
						GameStateHub.ButtonPressedEvent += ButtonPressed;
						// GameStateHub.ButtonReleasedEvent += ButtonReleased;
						GameStateHub.RotaryRotateDownEvent += RotaryRotateDown;
						GameStateHub.RotaryRotateUpEvent += RotaryRotateUp;
				}

				private async void ButtonPressed(object sender, EventArgsWithId e)
				{
						if (e.Id <= 10)
						{
								_logger.LogInformation($"ButtonPressed {e.Id}");
								var note = (byte)(59 + e.Id);

								if (_pressedButtons.Contains(e.Id))
								{
										_pressedButtons.RemoveAll(item => item == e.Id);
										_midi.NoteOff(note: note);
										await _hub.Clients.All.Off(e.Id);
								}
								else
								{
										_pressedButtons.Add(e.Id);
										_midi.NoteOn(note: note, 127, (byte)_rotaryButton[0]);
										await _hub.Clients.All.On(e.Id);
								}
						}
				}

				private async void RotaryRotateDown(object sender, EventArgsWithId e)
				{
						_logger.LogInformation($"RotaryRotateDown {e.Id}");
						_rotaryButton[e.Id] = (_rotaryButton[e.Id] == 0) ? (_instruments.Length - 1) : (_rotaryButton[e.Id] - 1);
						await _hub.Clients.All.Down(e.Id);
						foreach (var button in _pressedButtons)
						{
								var note = (byte)(59 + button);
								_midi.NoteOff(note);
								_midi.NoteOn(note, 127, (byte)_instruments[_rotaryButton[e.Id]]);
						}
				}

				private async void RotaryRotateUp(object sender, EventArgsWithId e)
				{
						_logger.LogInformation($"RotaryRotateDown {e.Id}");
						_rotaryButton[e.Id] = (_rotaryButton[e.Id] == (_instruments.Length - 1)) ? 0 : (_rotaryButton[e.Id] + 1);
						await _hub.Clients.All.Up(e.Id);
						foreach (var button in _pressedButtons)
						{
								var note = (byte)(59 + button);
								_midi.NoteOff(note);
								_midi.NoteOn(note, 127, (byte)_instruments[_rotaryButton[e.Id]]);
						}
				}

				public async void Dispose()
				{
						for (var i = 1; i <= 10; i++)
						{
								await _hub.Clients.All.Off(i);
						}

						_midi.AllNotesOff();

						GameStateHub.ButtonPressedEvent -= ButtonPressed;
						// GameStateHub.ButtonReleasedEvent += ButtonReleased;
						GameStateHub.RotaryRotateDownEvent -= RotaryRotateDown;
						GameStateHub.RotaryRotateUpEvent -= RotaryRotateUp;
				}

				public void StartAsync(QuestionModel question)
				{
						_logger.LogInformation("StartAsync");
						_question = question;
				}

				public bool CheckAnswer()
				{
						var buttonRowOk = _pressedButtons.Intersect(_question.Answer).Count() == _question.Answer.Count();
						// var rotaryButtons = _rotaryButtons
						return buttonRowOk;
				}
		}
}