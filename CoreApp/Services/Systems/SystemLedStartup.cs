using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Commons.Music.Midi;
using CoreApp.Hubs;
using CoreApp.Models;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;

namespace CoreApp.Services.Systems
{
		public class SystemLedStartup
		{
				private readonly IHubContext<GameStateHub, IGameStateHub> _hub;

				private readonly ILogger<SystemLedStartup> _logger;

				public SystemLedStartup(ILogger<SystemLedStartup> logger, IHubContext<GameStateHub, IGameStateHub> hub)
				{
						_logger = logger;
						_hub = hub;
				}

				private async void ButtonPressed(object sender, EventArgsWithId e)
				{
						await _hub.Clients.All.Off(e.Id);
						Task.Delay(500).Wait();
						await _hub.Clients.All.On(e.Id);
				}

				public async Task StartAsync()
				{
						_logger.LogInformation("StartAsync");
						for (var i = 1; i <= 10; i++)
						{
								await _hub.Clients.All.On(i);
								Task.Delay(200).Wait();
						}
						for (var i = 1; i <= 10; i++)
						{
								await _hub.Clients.All.Off(i);
								Task.Delay(200).Wait();
						}
				}
		}
}
