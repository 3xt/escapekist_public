using System;
using System.Threading.Tasks;
using CoreApp.Models;

namespace CoreApp.Services.Systems
{
	public interface ISystem : IDisposable
	{
		void StartAsync(QuestionModel model);

		bool CheckAnswer();
	}
}