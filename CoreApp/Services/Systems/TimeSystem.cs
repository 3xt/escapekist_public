using System;
using System.Threading.Tasks;
using System.Timers;
using CoreApp.Hubs;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;

namespace CoreApp.Services.Systems
{
		public class TimeSystem : IDisposable
		{
				private readonly ILogger<TimeSystem> _logger;

				private readonly IHubContext<GameStateHub, IGameStateHub> _hub;

				private Timer _gameTimer;

				private Timer _secondsLeftTimer;

				private int _secondsLeft = 0;

				public event EventHandler<EventArgs> TimeEnded;

				public TimeSystem(ILogger<TimeSystem> logger, IHubContext<GameStateHub, IGameStateHub> hub)
				{
						_logger = logger;
						_hub = hub;
				}

				public void Start(int forSeconds)
				{
						_secondsLeft = forSeconds;

						_secondsLeftTimer = new Timer(1000);
						_secondsLeftTimer.Elapsed += async (sender, e) =>
						{
								_secondsLeft -= 1;
								await _hub.Clients.All.Time(_secondsLeft);
						};

						_secondsLeftTimer.Start();
						StartGameTimer(forSeconds);

				}

				public void Stop()
				{
						_gameTimer.Stop();
						_gameTimer.Dispose();
						_secondsLeftTimer.Stop();
						_secondsLeftTimer.Dispose();
				}

				public int GetTimeLeft()
				{
						return _secondsLeft;
				}

				public void DecrementTime(int seconds = 30)
				{
						_secondsLeft = (_secondsLeft > seconds) ? _secondsLeft - seconds : 0;
						if (_secondsLeft > 0)
						{
								_gameTimer.Stop();
								_gameTimer.Dispose();
								StartGameTimer(_secondsLeft);
						}
						else
						{
								TimeEnded?.Invoke(this, new EventArgs());
						}
				}

				private void StartGameTimer(int forSeconds)
				{
						var gameTimerInMilliseconds = forSeconds * 1000;
						_gameTimer = new Timer(gameTimerInMilliseconds);
						_gameTimer.Elapsed += (sender, e) =>
						{
								TimeEnded?.Invoke(this, new EventArgs());
						};

						_gameTimer.Start();
				}

				public void Dispose()
				{
						_logger.LogInformation("Dispose");
						_gameTimer.Dispose();
						_secondsLeftTimer.Dispose();
				}
		}
}