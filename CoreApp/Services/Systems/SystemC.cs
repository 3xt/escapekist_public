using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreApp.Hubs;
using CoreApp.Models;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;

namespace CoreApp.Services.Systems
{
		public class SystemC : ISystem
		{
				private readonly IHubContext<GameStateHub, IGameStateHub> _hub;

				private readonly MidiService _midi;

				private readonly ILogger<SystemC> _logger;

				private QuestionModel _question;

				private List<int> _pressedButtons = new List<int>();

				private List<int> _rotaryButtons = new List<int> { 18, 18 };

				public SystemC(ILogger<SystemC> logger, MidiService midi, IHubContext<GameStateHub, IGameStateHub> hub)
				{
						_logger = logger;
						_hub = hub;
						_midi = midi;
						GameStateHub.ButtonPressedEvent += ButtonPressed;
						// GameStateHub.ButtonReleasedEvent += ButtonReleased;
						GameStateHub.RotaryRotateDownEvent += RotaryRotateDown;
						GameStateHub.RotaryRotateUpEvent += RotaryRotateUp;
				}

				private async void ButtonPressed(object sender, EventArgsWithId e)
				{
						if (e.Id <= 10)
						{
								_logger.LogInformation($"ButtonPressed {e.Id}");
								var note = (byte)(59 + e.Id);

								if (_pressedButtons.Contains(e.Id))
								{
										_pressedButtons.RemoveAll(item => item == e.Id);
										_midi.NoteOff(note: note);
										await _hub.Clients.All.Off(e.Id);
								}
								else
								{
										_pressedButtons.Add(e.Id);
										_midi.NoteOn(note: note, 127, (byte)_rotaryButtons[0]);
										await _hub.Clients.All.On(e.Id);
								}
						}
				}

				private async void RotaryRotateDown(object sender, EventArgsWithId e)
				{
						_logger.LogInformation($"RotaryRotateDown {e.Id}");
						_rotaryButtons[e.Id] = (_rotaryButtons[e.Id] == 0) ? 0 : (_rotaryButtons[e.Id] - 1);
						await _hub.Clients.All.Down(e.Id);
						foreach (var button in _pressedButtons)
						{
								var note = (byte)(59 + button);
								_midi.NoteOff(note);
								_midi.NoteOn(note, 127, (byte)_rotaryButtons[0]);
						}
				}

				private async void RotaryRotateUp(object sender, EventArgsWithId e)
				{
						_logger.LogInformation($"RotaryRotateDown {e.Id}");
						_rotaryButtons[e.Id] = (_rotaryButtons[e.Id] == 127) ? 127 : (_rotaryButtons[e.Id] + 1);
						await _hub.Clients.All.Up(e.Id);
						foreach (var button in _pressedButtons)
						{
								var note = (byte)(59 + button);
								_midi.NoteOff(note);
								_midi.NoteOn(note, 127, (byte)_rotaryButtons[0]);
						}
				}

				public async void Dispose()
				{
						for (var i = 1; i <= 10; i++)
						{
								await _hub.Clients.All.Off(i);
						}

						_midi.AllNotesOff();

						GameStateHub.ButtonPressedEvent -= ButtonPressed;
						// GameStateHub.ButtonReleasedEvent += ButtonReleased;
						GameStateHub.RotaryRotateDownEvent -= RotaryRotateDown;
						GameStateHub.RotaryRotateUpEvent -= RotaryRotateUp;
				}

				public void StartAsync(QuestionModel question)
				{
						_logger.LogInformation("StartAsync");
						_question = question;
				}

				public bool CheckAnswer()
				{
						var buttonRowOk = _pressedButtons.Intersect(_question.Answer).Count() == _question.Answer.Count();
						return buttonRowOk;
				}
		}
}