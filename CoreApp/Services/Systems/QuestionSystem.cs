using System.Text.Json.Serialization;

namespace CoreApp.Systems
{
		[JsonConverter(typeof(JsonStringEnumConverter))]
		public enum QuestionSystem
		{
				SystemA,
				SystemA1,
				SystemB,
				SystemB1,
				SystemC,
				SystemC1,
		}
}