using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Commons.Music.Midi;
using CoreApp.Hubs;
using CoreApp.Models;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;

namespace CoreApp.Services.Systems
{
		public class SystemA : ISystem
		{
				private readonly IHubContext<GameStateHub, IGameStateHub> _hub;

				private readonly MidiService _midi;

				private readonly ILogger<SystemA> _logger;

				private QuestionModel _question;

				private readonly List<int> _pressedButtons = new List<int>();

				public SystemA(ILoggerFactory factory, MidiService midi, IHubContext<GameStateHub, IGameStateHub> hub)
				{
						_logger = factory.CreateLogger<SystemA>();
						_hub = hub;
						_midi = midi;
						GameStateHub.ButtonPressedEvent += ButtonPressed;
				}

				protected async void ButtonPressed(object sender, EventArgsWithId e)
				{
						if (e.Id <= 10)
						{
								_logger.LogInformation($"ButtonPressed {e.Id}");

								if (_pressedButtons.Contains(e.Id))
								{
										_pressedButtons.RemoveAll(item => item == e.Id);
										_ = Task.Run(() => _midi.PlayButtonReleaseSound());
										await _hub.Clients.All.Off(e.Id);
								}
								else
								{
										_pressedButtons.Add(e.Id);
										_ = Task.Run(() => _midi.PlayButtonSetSound());
										await _hub.Clients.All.On(e.Id);
								}
						}
				}

				public async void Dispose()
				{
						for (var i = 1; i <= 10; i++)
						{
								await _hub.Clients.All.Off(i);
						}

						_midi.AllNotesOff();
						GameStateHub.ButtonPressedEvent -= ButtonPressed;
				}

				public void StartAsync(QuestionModel question)
				{
						_logger.LogInformation("StartAsync");
						_question = question;
				}

				public bool CheckAnswer()
				{
						var buttonRowOk = _pressedButtons.Intersect(_question.Answer).Count() == _question.Answer.Count();
						return buttonRowOk;
				}
		}
}