using System;
using CoreApp.Systems;

namespace CoreApp.Services.Systems
{
		public static class SystemResolver
		{
				public static ISystem GetSystem(IServiceProvider serviceProvider, QuestionSystem system)
				{
						return system switch
						{
								QuestionSystem.SystemA => (SystemA)serviceProvider.GetService(typeof(SystemA)),
								QuestionSystem.SystemA1 => (SystemA)serviceProvider.GetService(typeof(SystemA)),
								QuestionSystem.SystemB => (SystemB)serviceProvider.GetService(typeof(SystemB)),
								QuestionSystem.SystemB1 => (SystemB)serviceProvider.GetService(typeof(SystemB)),
								QuestionSystem.SystemC => (SystemC)serviceProvider.GetService(typeof(SystemC)),
								QuestionSystem.SystemC1 => (SystemC)serviceProvider.GetService(typeof(SystemC)),
								_ => null,
						};
				}
		}
}