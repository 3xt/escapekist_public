using System;
using System.Text.Json.Serialization;
using CoreApp.Hubs;
using CoreApp.Models;
using CoreApp.Services;
using CoreApp.Services.Systems;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog;
using Serilog.Sinks.SignalR.Core.Extensions;
using Serilog.Sinks.SignalR.Core.Interfaces;

namespace CoreApp
{
		public class Startup
		{
				private readonly IHostEnvironment _env;

				public Startup(IConfiguration configuration, IHostEnvironment env)
				{
						Configuration = configuration;
						_env = env;

						Log.Logger = new LoggerConfiguration()
							.ReadFrom.Configuration(Configuration)
							.CreateLogger();
				}

				public IConfiguration Configuration { get; }

				// This method gets called by the runtime. Use this method to add services to the container.
				public void ConfigureServices(IServiceCollection services)
				{
						services.AddOptions();
						services.Configure<AppSettings>(Configuration.GetSection("AppSettings"));

						services.AddSingleton<GameService, GameService>();
						services.AddTransient<TimeSystem, TimeSystem>();
						services.AddTransient<SystemLedStartup, SystemLedStartup>();
						services.AddTransient<SystemA, SystemA>();
						services.AddTransient<SystemB, SystemB>();
						services.AddTransient<SystemC, SystemC>();
						services.AddTransient<MidiService, MidiService>();
						//services.AddHostedService<Worker>();

						services.AddControllers().AddJsonOptions(opts =>
							{
									opts.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());
							});

						services.AddCors(options =>
						{
								// this defines a CORS policy called "default"
								options.AddPolicy("corsPolicy", policy =>
								{
										policy
				//.WithOrigins()
				.WithOrigins("http://raspberrypi:8080")
				.AllowAnyHeader()
				.SetIsOriginAllowed((host) => true)
				.AllowAnyMethod()
				.AllowCredentials();
								});
						});

						services.AddSignalR();
				}

				// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
				public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IServiceProvider sp)
				{
						if (env.IsDevelopment())
						{
								app.UseDeveloperExceptionPage();
						}

						// start the singleton
						app.ApplicationServices.GetService<GameService>();

						app.UseRouting();

						app.UseCors("corsPolicy");

						app.UseAuthorization();

						// Build an intermediate service provider
						var hub = sp.GetService<IHubContext<SerilogHub, ISerilogHub>>();
						Log.Logger = new LoggerConfiguration()
						.ReadFrom.Configuration(Configuration)
							.WriteTo.SignalR(hub, null, null, null, null)
							.CreateLogger();

						app.UseEndpoints(endpoints =>
						{
								endpoints.MapControllers();
								endpoints.MapHub<GameStateHub>("/gameStateHub");
								endpoints.MapHub<SerilogHub>("/log");
						});
				}
		}
}
