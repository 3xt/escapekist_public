using System.Collections.Generic;

namespace CoreApp.Models
{
	public class HintModel
	{
		public string Title { get; set; }

		public string Text { get; set; }

		// Cost in seconds
		public int CostInSeconds { get; set; }

	}
}