using System;

namespace CoreApp.Models
{
	public class EventArgsWithId : EventArgs
	{
		public int Id { get; set; }
	}
}