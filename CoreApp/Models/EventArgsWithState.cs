using System;

namespace CoreApp.Models
{
	public class EventArgsWithState : EventArgs
	{
		public byte State { get; set; }
	}
}