using System.Collections.Generic;
using CoreApp.Models.GameEvents;

namespace CoreApp.Models
{
		public class GameModel
		{
				public int TimeInMinutes { get; set; } = 30;

				public string Title { get; set; }

				public string Text { get; set; }

				public List<QuestionModel> Questions { get; set; }

				public GameEventModel StartEvent { get; set; } = new GameEventModel { Image = null, Sound = new MidiPlayFileModel() };

				public GameEventModel EndEvent { get; set; } = new GameEventModel { Image = null, Sound = new MidiPlayFileModel() };

				public GameEventModel SuccessEvent { get; set; } = new GameEventModel { Image = null, Sound = new MidiPlayFileModel() };

				public GameEventModel WaitEvent { get; set; } = new GameEventModel { Image = null, Sound = new MidiPlayFileModel() };

				public GameEventModel FailureEvent { get; set; } = new GameEventModel { Image = null, Sound = new MidiPlayFileModel() };
		}
}