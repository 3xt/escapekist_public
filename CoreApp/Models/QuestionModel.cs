using System.Collections.Generic;
using System.Text.Json.Serialization;
using CoreApp.Systems;

namespace CoreApp.Models
{
	public class QuestionModel
	{
		public string Title { get; set; }

		public string Text { get; set; }

		public List<HintModel> Hints { get; set; }

		[JsonConverter(typeof(JsonStringEnumConverter))]
		public QuestionSystem System { get; set; }

		public List<int> Answer { get; set; }

		public MidiPlayFileModel IntroSound { get; set; }

		public string ImageName { get; set; }
	}
}