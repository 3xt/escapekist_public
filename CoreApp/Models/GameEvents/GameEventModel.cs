namespace CoreApp.Models.GameEvents
{
		public class GameEventModel
		{
				public string Image { get; set; }

				public MidiPlayFileModel Sound { get; set; }
		}
}