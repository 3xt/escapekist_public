using CoreApp.Services.Systems;

namespace CoreApp.Models
{
		public class GameQuestionModel
		{
				public string System { get; set; }

				public int Total { get; set; }

				public int Current { get; set; }

				public string Title { get; set; }

				public string Text { get; set; }

				public string Image { get; set; }
		}
}