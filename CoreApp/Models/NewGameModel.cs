using System.ComponentModel.DataAnnotations;

namespace CoreApp.Models
{
	public class NewGameModel
	{
		[Required]
		public string Name { get; set; }
	}
}