using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using Serilog.Sinks.SignalR.Core.Interfaces;

namespace CoreApp.Hubs
{
		public class SerilogHub : Hub<ISerilogHub>
		{
				private readonly ILogger<SerilogHub> _logger;

				public SerilogHub(ILogger<SerilogHub> logger)
				{
						_logger = logger;
						_logger.LogInformation("--> Started ");
				}
		}
}