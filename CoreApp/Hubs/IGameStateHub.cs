using System.Threading.Tasks;
using CoreApp.Models;
using CoreApp.Models.GameEvents;

namespace CoreApp.Hubs
{
		public interface IGameStateHub
		{
				Task On(int id);

				Task Off(int id);

				Task Down(int id);

				Task Up(int id);

				Task LedRowUpdate(byte state);

				Task Time(int secondsLeft);

				Task GameEnded(GameEventModel model);

				Task GameStarted(GameEventModel model);

				Task GamePaused(GameEventModel model);

				Task GameResumed();

				Task GameQuestion(GameQuestionModel model);

				Task GameQuestionSuccess(GameEventModel model);

				Task GameQuestionFailure(GameEventModel model);

				Task BigButtonPressed(int id);

				Task ButtonPressed(int id);

				Task ButtonReleased(int id);

				Task ButtonRowChanged(byte state);
		}
}
