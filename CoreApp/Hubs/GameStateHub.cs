using System;
using CoreApp.Models;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;

namespace CoreApp.Hubs
{
		public class GameStateHub : Hub<IGameStateHub>
		{
				/*
				A new instance of the hub class is created for each request, so hub classes cannot contain any state. Also, it is not possible to add instance events to a hub class and track the operations by subscribing to these events.
				To overcome this problem, two methods can be used:
				- Adding static state variables and events
				- Injecting a state object to each created hub instance, using dependency injection
				*/
				public static bool GamePaused = false;

				public static event EventHandler<EventArgs> TimeEndedEvent;
				public static event EventHandler<EventArgsWithId> BigButtonPressedEvent;
				// public static event EventHandler<EventArgsWithState> ButtonRowUpdatedEvent;
				public static event EventHandler<EventArgsWithId> ButtonPressedEvent;
				public static event EventHandler<EventArgsWithId> ButtonReleasedEvent;
				public static event EventHandler<EventArgsWithId> RotaryRotateDownEvent;
				public static event EventHandler<EventArgsWithId> RotaryRotateUpEvent;

				private readonly ILogger<GameStateHub> _logger;

				public GameStateHub(ILogger<GameStateHub> logger)
				{
						_logger = logger;
						_logger.LogDebug("--> Started ");
				}

				// These methods will be called from the signalR clients
				public void TimeEnded()
				{
						_logger.LogDebug($"TimeEnded");
						TimeEndedEvent?.Invoke(this, new EventArgs());
				}

				public void ButtonPressed(int id)
				{
						if (id == 16)
						{
								_logger.LogInformation($"BigButtonPressed: {id}, gamePaused: {GamePaused}");
								if (!GamePaused)
								{
										BigButtonPressedEvent?.Invoke(this, new EventArgsWithId { Id = id });
								}

						}
						else
						{
								_logger.LogInformation($"ButtonPressed {id}, gamePaused: {GamePaused}");
								if (!GamePaused)
								{
										ButtonPressedEvent?.Invoke(this, new EventArgsWithId { Id = id });
								}

						}
				}

				public void ButtonReleased(int id)
				{
						_logger.LogInformation($"ButtonReleased {id}, gamePaused: {GamePaused}");
						if (!GamePaused)
						{
								ButtonReleasedEvent?.Invoke(this, new EventArgsWithId { Id = id });
						}
				}

				public void RotaryRotateDown(int id)
				{
						_logger.LogInformation($"RotaryRotateDown {id}, gamePaused: {GamePaused}");
						if (!GamePaused)
						{
								RotaryRotateDownEvent?.Invoke(this, new EventArgsWithId { Id = id });
						}
				}

				public void RotaryRotateUp(int id)
				{
						_logger.LogInformation($"RotaryRotateUp {id}, gamePaused: {GamePaused}");
						if (!GamePaused)
						{
								RotaryRotateUpEvent?.Invoke(this, new EventArgsWithId { Id = id });
						}
				}
		}
}