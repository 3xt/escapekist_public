﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using CoreApp.Models;
using CoreApp.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.StaticFiles;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace CoreApp.Controllers
{
		[ApiController]
		[Route("[controller]")]
		public class ImageController : ControllerBase
		{
				private readonly ILogger<ImageController> _logger;
				private readonly AppSettings _settings;

				public ImageController(ILogger<ImageController> logger, IOptions<AppSettings> settings)
				{
						_logger = logger;
						_settings = settings.Value;

						// Create directory if not exist
						Directory.CreateDirectory(_settings.ImagePath);
				}

				[HttpPost("[action]")]
				public async Task<IActionResult> UploadFile(List<IFormFile> files)
				{
						var size = files.Sum(f => f.Length);

						foreach (var formFile in files)
						{
								if (formFile.Length > 0)
								{
										var filePath = Path.Combine(_settings.ImagePath, formFile.FileName);

										using (var stream = System.IO.File.Create(filePath))
										{
												await formFile.CopyToAsync(stream);
										}
								}
						}

						// Process uploaded files
						// Don't rely on or trust the FileName property without validation.

						return Ok(new { count = files.Count, size, /* filePath */ });
				}

				[HttpGet("[action]/{filename}")]
				public IActionResult Download(string filename)
				{
						var filePath = Path.Combine(_settings.ImagePath, filename);
						if (!System.IO.File.Exists(filePath))
						{
								_logger.LogInformation("File does not exist: {@filename}", filename);
								return NotFound();
						}

						var stream = new FileStream(filePath, FileMode.Open);

						new FileExtensionContentTypeProvider().TryGetContentType(filename, out var contentType);
						return File(stream, contentType, filename);
				}

				[HttpGet("[Action]")]
				public IActionResult FileList()
				{
						var dir = new DirectoryInfo(_settings.ImagePath);
						var files = dir.GetFiles();
						var test = files.Select(x => new { x.Name, x.Length, x.CreationTime });
						return Ok(test);
				}

				[HttpDelete("{filename}")]
				public IActionResult DeleteFile(string filename)
				{
						var filePath = Path.Combine(_settings.ImagePath, filename);
						System.IO.File.Delete(filePath);
						return Ok();
				}
		}
}
