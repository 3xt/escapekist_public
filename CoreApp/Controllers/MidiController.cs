﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using CoreApp.Models;
using CoreApp.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace CoreApp.Controllers
{
		[ApiController]
		[Route("[controller]")]
		public class MidiController : ControllerBase
		{
				private readonly ILogger<MidiController> _logger;
				private readonly MidiService _midiService;
				private readonly AppSettings _settings;

				public MidiController(ILogger<MidiController> logger, MidiService midiService, IOptions<AppSettings> settings)
				{
						_logger = logger;
						_midiService = midiService;
						_settings = settings.Value;

						// Create directory if not exist
						Directory.CreateDirectory(_settings.MidiPath);
				}

				[HttpGet("[action]")]
				public void PlaySound()
				{
						_midiService.PlaySound();
				}

				[HttpGet("[action]")]
				public void AllNotesOff()
				{
						_midiService.AllNotesOff();
				}

				[HttpGet("[action]")]
				public void PlayNote(byte note, int milliSeconds, byte velocity, InstrumentsEnum instrument)
				{
						_ = Task.Run(() => _midiService.PlayNote(note, milliSeconds, velocity, (byte)instrument));
				}

				[HttpGet("[action]")]
				public void PlayPercussion(int milliSeconds = 1000, byte velocity = 127, PercussionsEnum percussion = PercussionsEnum.ClosedHiHat)
				{
						_midiService.PlayPercussion(percussion, milliSeconds, velocity);
				}

				[HttpPost("[action]")]
				public async Task PlayFile([FromBody] MidiPlayFileModel model)
				{
						await _midiService.PlayMusic(model.Filename, model.Milliseconds, model.OffSetMilliseconds);
				}

				[HttpPost("[action]")]
				public async Task<IActionResult> UploadFile(List<IFormFile> files)
				{
						var size = files.Sum(f => f.Length);

						foreach (var formFile in files)
						{
								if (formFile.Length > 0)
								{
										var filePath = Path.Combine(_settings.MidiPath, formFile.FileName);

										using (var stream = System.IO.File.Create(filePath))
										{
												await formFile.CopyToAsync(stream);
										}
								}
						}

						// Process uploaded files
						// Don't rely on or trust the FileName property without validation.

						return Ok(new { count = files.Count, size, /* filePath */ });
				}


				[HttpGet("[action]/{filename}")]
				public IActionResult Download(string filename)
				{
						var filePath = Path.Combine(_settings.MidiPath, filename);
						var stream = new FileStream(filePath, FileMode.Open);
						return File(stream, "music/midi", filename);
				}

				[HttpGet("[Action]")]
				public IActionResult FileList()
				{
						var dir = new DirectoryInfo(_settings.MidiPath);
						if (!dir.Exists) dir.Create();
						var files = dir.GetFiles();
						var test = files.Select(x => new { x.Name, x.Length, x.CreationTime });
						return Ok(test);
				}

				[HttpDelete("{filename}")]
				public IActionResult DeleteFile(string filename)
				{
						var filePath = Path.Combine(_settings.MidiPath, filename);
						System.IO.File.Delete(filePath);
						return Ok();
				}

				[HttpGet("[Action]")]
				public IActionResult Instruments()
				{
						var instruments = Enum.GetValues(typeof(InstrumentsEnum)).Cast<InstrumentsEnum>().ToList();
						return new OkObjectResult(instruments);
				}

				[HttpGet("[Action]")]
				public IActionResult Percussions()
				{
						var percussions = Enum.GetValues(typeof(PercussionsEnum)).Cast<PercussionsEnum>().ToList();
						return new OkObjectResult(percussions);
				}
		}
}
