﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using CoreApp.Models;
using CoreApp.Services;
using CoreApp.Systems;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace CoreApp.Controllers
{
		[ApiController]
		[Route("[controller]")]
		public class GameController : ControllerBase
		{
				private readonly ILogger<GameController> _logger;
				private readonly AppSettings _settings;

				private readonly GameService _gameservice;

				public GameController(ILogger<GameController> logger, IOptions<AppSettings> settings, GameService gameservice)
				{
						_settings = settings.Value;
						_logger = logger;
						_gameservice = gameservice;
				}

				[HttpGet("[Action]")]
				public IActionResult FileList()
				{
						var dir = new DirectoryInfo(_settings.GamePath);
						if (!dir.Exists) dir.Create();
						var files = dir.GetFiles().OrderBy(x => x.Name);
						var test = files.Select(x => new { ShortName = Path.GetFileNameWithoutExtension(x.Name), x.Name, x.Length, x.CreationTime });
						return Ok(test);
				}

				[HttpGet("[Action]/{filename}")]
				public async Task<IActionResult> Start(string filename)
				{
						_logger.LogInformation("Start game: {@filename}", filename);
						await _gameservice.StartGame(filename);
						return Ok();
				}

				[HttpGet("[Action]")]
				public IActionResult Shutdown()
				{
						_logger.LogInformation("Starting shutdown");
						Console.WriteLine("Starting shutdown");

						var output = "sudo shutdown -h now".Bash();
						return Ok();
				}

				[HttpPost("[action]")]
				public async Task<IActionResult> UploadFile(List<IFormFile> files)
				{
						var size = files.Sum(f => f.Length);

						foreach (var formFile in files)
						{
								if (formFile.Length > 0)
								{
										var filePath = Path.Combine(_settings.GamePath, formFile.FileName);

										using (var stream = System.IO.File.Create(filePath))
										{
												await formFile.CopyToAsync(stream);
										}
								}
						}

						// Process uploaded files
						// Don't rely on or trust the FileName property without validation.

						return Ok(new { count = files.Count, size, /* filePath */ });
				}

				[HttpPost("{name}")]
				public async Task<IActionResult> New(string name, [FromBody] GameModel model)
				{
						var filePath = Path.Combine(_settings.GamePath, $"{name}.json");

						var json = JsonSerializer.Serialize(model);
						await System.IO.File.WriteAllTextAsync(filePath, json);
						return Ok();
				}

				[HttpDelete("{filename}")]
				public IActionResult DeleteFile(string filename)
				{
						var filePath = Path.Combine(_settings.GamePath, filename);
						System.IO.File.Delete(filePath);
						return Ok();
				}

				[HttpGet("[action]/{filename}")]
				public IActionResult Download(string filename)
				{
						var filePath = Path.Combine(_settings.GamePath, filename);
						var stream = new FileStream(filePath, FileMode.Open);
						return File(stream, "application/octet-stream", filename);
				}

				[HttpGet("{filename}")]
				public async Task<IActionResult> Get(string filename)
				{
						var filePath = Path.Combine(_settings.GamePath, filename);
						var jsonString = await System.IO.File.ReadAllTextAsync(filePath);
						var model = JsonSerializer.Deserialize<GameModel>(jsonString);

						return new OkObjectResult(model);
				}

				[HttpPut("{filename}")]
				public async Task<IActionResult> Put(string filename, [FromBody] GameModel model)
				{
						var filePath = Path.Combine(_settings.GamePath, filename);
						var json = JsonSerializer.Serialize(model);
						await System.IO.File.WriteAllTextAsync(filePath, json);
						return Ok();
				}

				[HttpGet("Systems")]
				public IActionResult Systems()
				{
						var enumVals = new List<object>();

						foreach (var item in Enum.GetValues(typeof(QuestionSystem)))
						{

								enumVals.Add(new
								{
										id = (int)item,
										name = item.ToString()
								});
						}

						return Ok(enumVals);
				}
		}
}
